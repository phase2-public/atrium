#!/usr/bin/env bash
# After downloading database dump artifact, run this to update repo

# Artifacts exist for both "quick" and "release" build jobs. Override with $1.
ATRIUM_BUILD="standard"
# Path artifact was downloaded. Override with $2
ARTIFACT_PATH=~/Downloads

#-------------------------------
CURDIR=`pwd -P`
cd profile

# Process command line arguments
ARTIFACT_FILE="${ARTIFACT_PATH}/openatrium_${ATRIUM_BUILD}.zip"
if [ ! -z $2 ]; then
  ARTIFACT_FILE="$2/openatrium_$1.zip"
elif [ ! -z $1 ]; then
  if [[ $1 = "quick" || $1 = "standard" ]]; then
    ARTIFACT_FILE="${ARTIFACT_PATH}/openatrium_$1.zip"
  else
    ARTIFACT_FILE="$1/openatrium_${ATRIUM_BUILD}.zip"
  fi
fi

if [ ! -e ${ARTIFACT_FILE} ]; then
  echo "Must first download the artifact from the GitLab pipeline job into: ${ARTIFACT_FILE}"
  exit 1
fi

# Extract and copy db dump into "profile" repo
echo "Updating files in profile/db..."
unzip -o "${ARTIFACT_FILE}" -d db

# Commit changes
git add db/openatrium.mysql
git commit -m "Update db for release"

# Ready for release
git status
YELLOW="\033[0;33m"
NORMAL="\033[0;0m"
echo "------"
printf "${YELLOW}cd profile${NORMAL} to push release to drupal.org\n"
echo "Follow release checklist at https://wiki.phase2technology.com/display/OA/Open+Atrium+2.0+-+Release+Checklist"

cd ${CURDIR}
