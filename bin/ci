#!/bin/bash
## Run 'kubectl' commands and shell into kubernetes cluster.
## Usage: fin ci [-n project_namespace] [-p project] [command] [arguments]
## If 'project_namespace' is not specified, the PROJECT_NAME env var is used.
## Use -p to override the PROJECT_NAME within the same namespace.
## Commands:
##   login : one-time login with google
##   bash [environment] [container] : connect bash shell to specified environment
##     'environment' should be qa, test, master, develop, issue-1234, etc
##     'container' defaults to 'cli' and can be either 'web' or 'cli'.
##     If no environment is specified, current git branch env is used.
##   list : list the pods in current project
##   url [environment] : list the URLs assigned to the environment.
##     If no environment specified, the current branch is used.
##   kube : send arguments directly to kubectl
##   delete [environment] : delete kubernetes objects for an environment.
## If no Command is specified, simply sets the default kubernetes namespace.
##
## This does not require docksal and can be run directly

source "$(dirname $0)/.helper"
cd ${PROJECT_ROOT}

set -e

function finish {
  # Exit handler. Cleanup goes here.
  cluster_restore
  docker_stop
}
trap finish EXIT

# Execute a command either locally or within docker container.
# First argument can be options passed to docker-exec, ignored locally.
# For example: docker_exec -it bash
docker_exec () {
  local options=""
  if [[ "$1" == "-"* ]]; then
    options="$1"
    shift
  fi
  if [[ $USE_DOCKER -eq 1 ]]; then
    # If using docker, run in container with options
    docker exec ${options} ${CONTAINER_NAME} $@
  else
    # Otherwise run locally
    $@
  fi
}

# Save current kubectl context and set it to the devcloud cluster.
# Set project namespace.
cluster_connect () {
  # GitLab CI is already connected to cluster
  if [[ -z "$CI" ]]; then
    SAVED_CONTEXT=$(${KUBECTL} config current-context || echo "")

    # This is the context created by get-credentials above.
    KUBE_CONTEXT="gke_phase2-devcloud_us-east4-b_devcloud"

    # Set up credentials for cluster so we can use kubectl in devcloud.
    ${GCLOUD} container clusters get-credentials devcloud --zone us-east4-b --project phase2-devcloud 2>/dev/null >/dev/null
    # Use the context within container. Needed to set cache so local doesn't fail
    docker_exec ${KUBECTL} config use-context ${KUBE_CONTEXT} >/dev/null
    # Use the context locally
    ${KUBECTL} config use-context ${KUBE_CONTEXT} >/dev/null

    if [ ${NAMESPACE} ]; then
      # Set the default namespace for this context
      ${KUBECTL} config set-context ${KUBE_CONTEXT} --namespace=${NAMESPACE} >/dev/null

      # If no ci command was specified, just change the context and namespace.
      if [[ -z "$CI_CMD" ]]; then
        if [[ "$SAVED_CONTEXT" != "$KUBE_CONTEXT" ]]; then
          if [[ $QUIET -eq 0 ]]; then echo "Setting kubectl context to '${KUBE_CONTEXT}'"; fi
        fi
        if [[ $QUIET -eq 0 ]]; then echo "Setting default namespace to '${NAMESPACE}'"; fi
        # Prevent cluster_restore from changing the context back again.
        SAVED_CONTEXT=""
      fi
    fi

    if [[ "$KUBE_CONTEXT" == "$SAVED_CONTEXT" ]]; then
      # No need to restored saved context if it already matches current.
      SAVED_CONTEXT=""
    fi

  else
    # Otherwise in CI, just set the gcloud authentication
    ${GCLOUD} container clusters get-credentials devcloud --zone us-east4-b --project phase2-devcloud 2>/dev/null >/dev/null
  fi
}

# Restore the previously saved kubectl context
cluster_restore () {
  # GitLab CI is already connected to cluster
  if [[ -z "$CI" ]]; then
    if [[ ! -z "$SAVED_CONTEXT" ]]; then
      ${KUBECTL} config use-context ${SAVED_CONTEXT} >/dev/null
    fi
  fi
}

docker_start () {
  if [[ -z $CONTAINER_NAME ]]; then
    # Create unique container name for this script instance.
    CONTAINER_NAME="gkh-container-$(date +%s)"

    # Start gcloud-kubectl container in background.
      # --volume ${PWD}:/workspace:ro
      # Mount current dir in case it's needed
      # --volume ~/.config/gcloud:/home/gkh/.config/gcloud
      # IMPORTANT: Mount the users gcloud config for authentication.
      # --env-file ${PROJECT_ROOT}/.env
      # Pass project variables to environment.
      # -td \
      # Background process with terminal
      # --rm \
      # Remove when complete.
    docker run \
      --volume ${PROJECT_ROOT}:/workspace \
      --workdir /workspace \
      --volume ~/.config/gcloud:/home/gkh/.config/gcloud \
      --volume ~/.kube:/home/gkh/.kube \
      --env-file ${PROJECT_ROOT}/.env \
      -td \
      --rm \
      --name $CONTAINER_NAME \
      kiwigrid/gcloud-kubectl-helm:latest /bin/bash >/dev/null
  fi
}

docker_stop () {
  if [[ ! -z $CONTAINER_NAME ]]; then
    # Ensure container is stopped and removed.
    docker kill $CONTAINER_NAME >/dev/null
  fi
}

# Get the current branch from git and convert to release name.
get_branch () {
  BRANCH="$1"
  if [ -z "$BRANCH" ]; then
    # If no branch specified, use current branch name.
    BRANCH="$(cd ${GIT_ROOT};git symbolic-ref HEAD 2>/dev/null || echo '')"
    BRANCH=${BRANCH##refs/heads/}
    BRANCH=${BRANCH//\//-}
    if [[ $BRANCH =~ ^[A-Za-z]+\-([0-9]+) ]]; then
      # Handle issue branch ingress name.
      ISSUE_NUM="${BASH_REMATCH[1]}"
      if [[ ! -z "${ISSUE_NUM}" ]]; then
        BRANCH="issue-${ISSUE_NUM}"
      fi
    fi
    if [ -z "$BRANCH" ]; then
      printf "$(WARN_SLUG) Cannot determine branch name as default pod.\n"
      exit
    fi
  fi
  echo ${BRANCH} | cut -c 1-32 | tr '[:upper:]' '[:lower:]'
}

# Set variable defaults
QUIET=0
NAMESPACE=${KUBE_NAMESPACE:-${PROJECT_NAME}}

if [[ ! -z "$GIT_ROOT" && "$GIT_ROOT" != "$PROJECT_ROOT" ]]; then
  # If we are in a submodule, determine project name from git remote.
  # This overrides the PROJECT_NAME but not the NAMESPACE.
  GIT_REMOTE=$(cd ${GIT_ROOT};git remote get-url origin)
  if [[ "$GIT_REMOTE" =~ ([A-Za-z0-9_\-]+)".git" ]]; then
    PROJECT_NAME="${BASH_REMATCH[1]}"
  fi
fi

# Parse comment line options
while [[ "$1" == "-"* ]]; do
  case $1 in
    -n) # Namespace
      shift
      NAMESPACE="$1"
      shift
      ;;

    -p) # Project name
      shift
      PROJECT_NAME="$1"
      shift
      ;;

    --debug)
      shift
      set -x
      ;;

    --quiet)
      shift
      QUIET=1
      ;;

  esac
done
N_NAMESPACE=$([ ${NAMESPACE} ] && echo "-n ${NAMESPACE}" || echo "")

# Get main script command name
if [[ ! -z "$1" ]]; then
  CI_CMD="$1"
  shift
fi

# NOTE: kubectl is installed by Docker Desktop so should always be local.
KUBECTL="kubectl"

USE_DOCKER=0
GCLOUD=$(which gcloud || echo "")
if [[ -z "$CI" ]]; then
  # If no local gcloud, then use docker container.
  if [[ -z "$GCLOUD" ]]; then
    USE_DOCKER=1
    GCLOUD="docker_exec -it gcloud"
  fi
fi

if [[ $USE_DOCKER -eq 1 ]]; then
  # Ensure Google auth folder exists before mounting it into container.
  if [ ! -e ~/.config/gcloud ]; then
    mkdir -p ~/.config/gcloud
  fi
  docker_start
fi

# If access credentials don't exist yet, log into gcloud.
if [[ "$CI_CMD" == "login" || ! -e ~/.config/gcloud/credentials.db ]]; then
  # Execute gcloud login authentication.
  # Will be persisted in ~/.config/gcloud dir.
  ${GCLOUD} auth login
  if [[ "$CI_CMD" == "login" ]]; then exit; fi
fi

# "bin/ci bash kube" shells into the local kube container.
if [[ "$CI_CMD" == "bash" && "$1" == "kube" ]]; then
  docker_start
  echo_exec docker exec -it ${CONTAINER_NAME} bash
  exit
fi

# All of the rest of the commands require the K8s cluster.
cluster_connect

case "$CI_CMD" in
  "")  # No command
    ;;

  list)  # List pods in cluster for project.
    echo_exec ${KUBECTL} get pods ${N_NAMESPACE}
    ;;

  bash)  # Shell into an environment
    # If cli/web specified without branch name, use current branch.
    if [[ "$1" == "cli" || "$1" == "web" ]]; then
      BRANCH=""
      CONTAINER="$1"
    else
      BRANCH="$1"
      CONTAINER="$2"
    fi
    RELEASE=${PROJECT_NAME}-$(get_branch $BRANCH)

    if [[ -z "$CONTAINER" || "$CONTAINER" == "cli" || "$CONTAINER" == "web" ]]; then
      # Determine the name of the webcontainer pod for this namespace.
      PODNAME=$(${KUBECTL} get pods -o name ${N_NAMESPACE} -l release=${RELEASE},webcontainer=true | cut -f2 -d/)
      if [[ -z "${PODNAME}" ]]; then
        # If didn't find a webcontainer, see if BRANCH is actually the full pod name
        PODNAME=$(${KUBECTL} get pods -o name ${N_NAMESPACE} --field-selector metadata.name="${BRANCH}" | cut -f2 -d/)
        if [[ ! -z "$PODNAME" ]]; then
          RELEASE="$BRANCH"
          CONTAINER=${CONTAINER:-cli}
        fi
      else
        CONTAINER=${CONTAINER:-cli}
      fi
    else
      # Look for the specific app service passed in $2, such as "mariadb"
      PODNAME=$(${KUBECTL} get pods -o name ${N_NAMESPACE} -l release=${RELEASE},app=${RELEASE}-$2 | cut -f2 -d/)
      if [[ -z "${PODNAME}" ]]; then
        # Look for pod with exact app name.
        PODNAME=$(${KUBECTL} get pods -o name ${N_NAMESPACE} -l release=${RELEASE},app=$2 | cut -f2 -d/)
      fi
      CONTAINER=""
    fi

    C_CONTAINER=""
    if [ ! -z "$CONTAINER" ]; then C_CONTAINER="-c $CONTAINER"; fi

    if [ -z "${PODNAME}" ]; then
      echo "ERROR: Cannot find pod for '${RELEASE}' environment"
      echo "Use 'fin ci list' to view available environments."
    else
      echo "Connecting to '${CONTAINER:-default}' container of '${RELEASE}' environment"
      echo "Type 'exit' to leave the shell."
      echo_exec ${KUBECTL} exec ${N_NAMESPACE} -it ${C_CONTAINER} ${PODNAME} bash
    fi
    ;;

  url)  # Display ingress URL for branch
    BRANCH="$1"
    RELEASE=${PROJECT_NAME}-$(get_branch $BRANCH)

    INGRESS=$(${KUBECTL} get ingress ${N_NAMESPACE} --field-selector metadata.name=${RELEASE} --no-headers=true)
    # Use tr to squeeze multiple spaces
    # then 'cut' to extract the 2nd HOST column
    # Then convert comma list into multiple lines
    echo "$INGRESS" | tr -s ' ' | cut -d ' ' -f2 | tr ',' '\n'
    ;;

  kube*)  # kube and kubectl within container.
    ${KUBECTL} ${N_NAMESPACE} $@
    ;;

  delete)  # delete K8s resources for a branch.
    BRANCH="$1"
    RELEASE=${PROJECT_NAME}-$(get_branch $BRANCH)
    # Get the objects to delete.
    OBJECTS=$(${KUBECTL} get po,deploy,sts,ing,secret,cm,pvc ${N_NAMESPACE} -l release=${RELEASE} \
      -o custom-columns=\TYPE:.kind,NAME:.metadata.name,CREATED:.metadata.creationTimestamp 2>/dev/null)
    OBJECT_COUNT=$(echo "$OBJECTS" | wc -l | tr -d '[:space:]')
    if [ $OBJECT_COUNT -le 1 ]; then
      # See if we got less than just the header line.
      echo "No objects found for environment '${RELEASE}'"
      exit
    fi
    printf "$(INFO_SLUG) Will delete the following objects:\n--------------------\n"
    echo "$OBJECTS"
    printf "\n\033[31mDo you REALLY want to delete all these objects?\033[37m y/[n] "
    read
    if [[ $REPLY =~ ^[Yy]$ ]]; then
      printf "${INFO_SLUG} Deleting Deployments\n"
      ${KUBECTL} delete deploy ${N_NAMESPACE} -l release=${RELEASE}
      printf "${INFO_SLUG} Deleting StatefulSets\n"
      ${KUBECTL} delete sts ${N_NAMESPACE} -l release=${RELEASE}
      printf "${INFO_SLUG} Deleting Services\n"
      ${KUBECTL} delete svc ${N_NAMESPACE} -l release=${RELEASE}
      printf "${INFO_SLUG} Deleting the Ingress\n"
      ${KUBECTL} delete ing ${N_NAMESPACE} -l release=${RELEASE}
      printf "${INFO_SLUG} Deleting Secrets\n"
      ${KUBECTL} delete secret ${N_NAMESPACE} -l release=${RELEASE}
      printf "${INFO_SLUG} Deleting ConfigMaps\n"
      ${KUBECTL} delete cm ${N_NAMESPACE} -l release=${RELEASE}
      printf "${INFO_SLUG} Deleting PersistentVolumeClaims\n"
      ${KUBECTL} delete pvc ${N_NAMESPACE} -l release=${RELEASE}
    fi
    ;;

  *)
    # Execute other command, such as gcloud.
    docker_exec ${CI_CMD} $@
    ;;

esac
