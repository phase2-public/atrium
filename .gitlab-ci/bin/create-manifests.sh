#!/usr/bin/env bash
# Common script to create the manifests for the Kubernetes cluster.
# Called from .gitlab-ci.yml to create manifests and cleanup environments.
# Pass branch to generate manifests for as optional argument.

ROOT_DIR=".gitlab-ci"
SCRIPT_DIR="${ROOT_DIR}/bin"
CHART_DIR="${ROOT_DIR}/chart"
MANIFEST_DIR=".manifests"

set -ex

# Remove any previous manifests from cache.
rm -rf ${MANIFEST_DIR}
mkdir ${MANIFEST_DIR}

if [ -z ${CI_DEPLOY_USER} ]; then
  echo "ERROR: CI_DEPLOY_USER not defined.  Add 'gitlab-deploy-token' with 'read-registry' access to your GitLab Settings > Repository > Deploy Tokens."
  exit 1
fi

# Compute the proper URL, RELEASE_NAME for the environment.
# Must use "source" here to bring in all the variables.
source ${SCRIPT_DIR}/release_name.sh $1

# Download chart dependencies.
helm repo add p2chartmuseum "https://chartmuseum.kube.fayze2.com/"
helm dependency build ${CHART_DIR}

# Create additional values from environment variables.
# Creates ${ROOT_DIR}/values-overrides.yaml for helm
# and ${CHART_DIR}/templates/env-config.yaml for config map.
${SCRIPT_DIR}/env-values.sh

# Generate the manifests from the chart templates.
helm template -f ${ROOT_DIR}/values-overrides.yaml \
  --name ${RELEASE_NAME} \
  --output-dir ${MANIFEST_DIR} \
  --debug \
  ${CHART_DIR}

