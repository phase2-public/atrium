#!/usr/bin/env bash
# Compute the proper URL and RELEASE_NAME for the environment.
#
# Call this via `source release_name.sh` to ensure variables are set.
#
# Used to convert a long branch name into a JIRA ticket number url.
# Example, for JIRA issue 123 (taken from PROJECT_ENV which has the branch name)
# RELEASE_NAME example: PROJECT-issue-123
# URL: issue-123

# Branch name with slashes converted to hyphens.
BRANCH_NAME="${CI_BUILD_REF_SLUG}"

# Truncate branch name to 32 chars.
export ENV_NAME=$(printf ${BRANCH_NAME} | cut -c 1-32 | sed 's/[-_.]$//')

# This is the URL used in .gitlab-ci.yml for branch environments.
export URL_ALT="${CI_COMMIT_SHORT_SHA}.${PROJECT_NAME}"

if [[ $BRANCH_NAME =~ ^[A-Za-z]+\-([0-9]+) ]]; then
  # Create better release name for branch based on JIRA ticket number.
  ISSUE_NUM="${BASH_REMATCH[1]}"
  if [[ ! -z "${ISSUE_NUM}" ]]; then
    ENV_NAME="issue-${ISSUE_NUM}"
    RELEASE_NAME="${PROJECT_NAME}-${ENV_NAME}"
  fi
fi

# Name used for actual deployment and pods.
export RELEASE_NAME="${PROJECT_NAME}-${ENV_NAME}"

# Remove subdomain if in master environment.
if [ ${ENV_NAME} == "master" ]; then
  export URL_ENV="${PROJECT_NAME}"
else
  export URL_ENV="${ENV_NAME}.${PROJECT_NAME}"
fi
