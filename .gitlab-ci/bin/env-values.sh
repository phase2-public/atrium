#!/usr/bin/env bash
# Sets chart values overrides from environment variables.
# Projects shouldn't need to modify this script!
#
# Add value overrides to the .gitlab-ci/chart/env-values.yaml file

# *********** Create the env-config config map k8s template ****************
#
# NOTE: We create config map via template file so GitLab has a manifest to
# remove config map when environment is cleaned.
# Cannot use `kubectl create configmap` since there is no way to specify labels.

inputEnvFile=".env"
outputConfigMap=".gitlab-ci/chart/templates/env-config.yaml"

inputValuesFile=".gitlab-ci/chart/env-values.yaml"
outputValuesFile=".gitlab-ci/values-overrides.yaml"

# Convert .env file of key=value into YAML lines.
# Also export the .env vars for use later in script.
env_to_yaml () {
  # Loop through .env file
  while read line; do
    # Look for uncommented lines for key=value
    if [[ "$line" =~ ^([^#].+)=(.+)$ ]]; then
      varName="${BASH_REMATCH[1]}"
      varValue="${BASH_REMATCH[2]}"
      # Check if value already has quotes.
      if [[ "$varValue" =~ ^\"(.+)\"$ ]]; then
        # Remove quotes since they are added later.
        varValue="${BASH_REMATCH[1]}"
      fi
      export $varName="$varValue"
      # Output indented YAML key: value line.
      printf "  $varName: \"$varValue\"\n"
    fi
  done <$inputEnvFile
}

envVars=$(env_to_yaml)
echo "apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ template \"chart.fullname\" . }}-env-config
  labels:
    app: {{ template \"chart.name\" . }}
    release: {{ .Release.Name }}
data:
${envVars}
" >$outputConfigMap

# *********** Create the override values for helm template *****************
#
# Output the environment variables into Values.yaml format.
# Uses .env variables exported above.
# Eval the lines in the input to expand variables.
eval "echo \"$(cat $inputValuesFile)\"" >$outputValuesFile
