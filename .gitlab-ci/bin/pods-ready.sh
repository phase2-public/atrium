#!/usr/bin/env bash

# Taken from https://github.com/zlabjp/kubernetes-scripts/blob/master/wait-until-pods-ready
# Modified by mpotter 2/25/2019 to take namespace argument.
#
# Copyright 2017, Z Lab Corporation. All rights reserved.
# Copyright 2017, Kubernetes scripts contributors
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.

# Usage: wait-until-pods-ready [-n NAMESPACE] RELEASE PERIOD INTERVAL
# RELEASE is used to filter the pods that need to be checked.
# Namespace of PROJECT_NAME is used if NAMESPACE is not set.
# PERIOD defaults to 240
# INTERVAL defaults to 10

NAMESPACE=""
if [ "$1" == "-n" ]; then
  NAMESPACE="-n ${2}"
  shift
  shift
elif [ ! -z ${PROJECT_NAME} ]; then
  NAMESPACE="-n ${PROJECT_NAME}"
fi
RELEASE="$1"

set -e

function __pods_ready() {
  kubectl ${NAMESPACE} rollout status deploy ${RELEASE} -w
}

function __list_pods() {
  kubectl get pods ${NAMESPACE} -l release=${RELEASE}
}

function __wait-until-pods-ready() {
  __pods_ready
  __list_pods
}

__wait-until-pods-ready
# vim: ft=sh :
