#!/usr/bin/env bash
# Execute a script as the docker user within GitLab
# This is needed because GitLab always executes jobs as "root"
# But the docksal/cli container is set to use the "docker" user.
set -e

printf "\033[33m[BASH] \033[37m"$(date +"%T")"\033[33;1m $@\033[0m\n"
# Need the "-l" to set up tools such as npm.
HOME=/home/docker sudo -E -u docker -- bash -lc "$@"
