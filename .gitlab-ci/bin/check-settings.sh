#!/usr/bin/env bash
# Verify required environment variables are set from GitLab CI project settings.

if [ -z "${PROJECT_NAME}" ]; then
  echo "The PROJECT_NAME environment variable must be set (in GitLab Settings > CI/CD)"
  exit 1
else
  echo "PROJECT_NAME set to ${PROJECT_NAME}"
fi

if [ -z "${AWS_ACCESS_KEY_ID}" ]; then
  echo "The AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_BUCKET environment variables must be set (in GitLab Settings > CI/CD)"
  exit 1
else
  echo "AWS_BUCKET set to ${AWS_BUCKET}"
fi
