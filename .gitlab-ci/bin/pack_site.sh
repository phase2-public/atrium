#!/usr/bin/env bash
# Common script to push a docker image to registry.
# Called from .gitlab-ci.yml in the "build" stage

INFO_SLUG() {
  printf "\033[33m[INFO] \033[37m"$(date +"%T")"\033[0m"
}

set -ex

# Compact the project for more efficient docker copy.
# Will be unpacked in the init-container process.
printf "$(INFO_SLUG) Compacting files into build artifact...\n"

# Fix permissions for Drupal settings.
if [[ -e "${REL_DOCROOT}/sites/default/settings.php" ]]; then
  chmod 444 "${REL_DOCROOT}/sites/default/settings.php"
  chmod 555 "${REL_DOCROOT}/sites/default"
fi

S3_PATH="sites/"
FILENAME="site-${CI_COMMIT_SHORT_SHA}.tgz"

# Exclude files not needed in deployed environment.
tar -cz \
  --exclude .git \
  --exclude .gitlab-ci \
  --exclude .docksal \
  --exclude node_modules \
  -f /tmp/${FILENAME} .

# Push site to s3
printf "$(INFO_SLUG) Pushing site to AWS s3 ${S3_PATH}${FILENAME}...\n"
aws s3 cp /tmp/${FILENAME} ${AWS_BUCKET}${S3_PATH}${FILENAME}
aws s3 ls ${AWS_BUCKET}${S3_PATH}

printf "$(INFO_SLUG) Image ready.\n"
