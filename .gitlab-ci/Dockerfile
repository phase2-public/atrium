ARG PHP_VERSION=php73
# This Dockfile is used for the web container in Kubernetes cluster
FROM outrigger/apache-php:${PHP_VERSION}
# Make the build arg accessible within the build stage
ARG PHP_VERSION

# Things to support drush.
# msmtp for MailHog
RUN yum install -y mysql which rsync msmtp

# Add composer installed tools to path.
ENV PATH=$PATH:/var/www/vendor/bin

# Allow PHP cli settings to be customized.
COPY ./.gitlab-ci/etc/99-project-php-settings.ini /etc/opt/remi/${PHP_VERSION}/php.d/99-project-php-settings.ini

# Allow php-fpm settings (that are not able to be set by env vars) to be included.
COPY ./.gitlab-ci/etc/www-custom.conf /etc/opt/remi/${PHP_VERSION}/php-fpm.d/www-custom.conf

# No need to copy codebase into image.
# Code will be packed and transferred via AWS S3
# then unpacked to container by the InitContainer script.
