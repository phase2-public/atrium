#!/usr/bin/env bash
#
# Build the full codebase.

INFO_SLUG() {
  printf "\033[33m[INFO] \033[37m"$(date +"%T")"\033[0m"
}

# Fix volume mount ownership.
#printf "$(INFO_SLUG) Setting file ownership...\n"
#sudo chown -R docker:docker /var/www

drush version

# Install drupal.org drush tools
drush dl --default-major=7 drupalorg_drush

# Build Atrium docroot from profile
printf "$(INFO_SLUG) Building Open Atrium...\n"
cd profile && ./build.sh ../docroot
printf "$(INFO_SLUG) Build complete.\n"
