<?php

/**
 * @file
 * CI environment overrides
 */

// Show errors including XDEBUG trace.
ini_set('display_errors', 1);
if (PHP_SAPI !== 'cli') {
  ini_set('html_errors', 1);
}

// Forcibly disable poorman's cron.
$conf['cron_safe_threshold'] = 0;

/**
 * Database configuration.
 */
$databases = array(
  'default' =>
    array(
      'default' =>
        array(
          'database' => getenv('MYSQL_DATABASE'),
          'username' => getenv('MYSQL_USER'),
          'password' => getenv('MYSQL_PASSWORD'),
          'host' => getenv('MYSQL_HOST'),
          'port' => getenv('MYSQL_PORT'),
          'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
          'driver' => 'mysql',
          'prefix' => '',
        ),
    ),
);

// Allow access to update.php.
$settings['update_free_access'] = TRUE;

/**
 * Show all error messages, with backtrace information.
 *
 * In case the error level could not be fetched from the database, as for
 * example the database connection failed, we rely only on this value.
 */
$config['system.logging']['error_level'] = 'verbose';

/**
 * Disable CSS and JS aggregation.
 */
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

/**
 * Temporary file path:
 *
 * A local file system path where temporary files will be stored. This
 * directory should not be accessible over the web.
 *
 * Note: Caches need to be cleared when this value is changed.
 *
 * See https://www.drupal.org/node/1928898 for more information
 * about global configuration override.
 */
$config['system.file']['path']['temporary'] = '/tmp';

/**
 * Trusted host configuration.
 *
 * See full description in default.settings.php.
 */
$settings['trusted_host_patterns'] = array(
  '^.+$',
);
