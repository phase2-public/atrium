{{/* vim: set filetype=mustache: */}}
{{/*
.Release.name comes from the name given to the `helm template` command in create.manifests.sh.
It typically looks like "$PROJECT_NAME-$PROJECT_ENV" where $PROJECT_ENV is the
name of the branch (master, issue-123, etc).

We truncate many values at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}

{{/*
Expand the name of the chart.
*/}}
{{- define "chart.name" -}}
{{- .Chart.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
*/}}
{{- define "chart.fullname" -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Render the mariadb container hostname.
*/}}
{{- define "chart.mariadb.fullname" -}}
{{- printf "%s-%s" .Release.Name "mariadb" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Render the memcached container hostname.
*/}}
{{- define "chart.memcached.fullname" -}}
{{- printf "%s-%s" .Release.Name "memcached" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a URL base for all project services
*/}}
{{- define "chart.ingress.url.base" -}}
{{- printf "%s.kube.fayze2.com" .Values.ingress.url.base -}}
{{- end -}}

{{/*
Create a second alternate URL (to match GitLab environment url)
*/}}
{{- define "chart.ingress.url.alt" -}}
{{- printf "%s.kube.fayze2.com" .Values.ingress.url.alt -}}
{{- end -}}
